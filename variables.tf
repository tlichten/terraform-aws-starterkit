# -*- coding: utf-8; mode: terraform; -*-

variable "starterkit_domain" {
  # This must not have a default.
}

variable "starterkit_region" {
  # This must not have a default.
}

variable "starterkit_vpc_cidr_block" {
  default = "10.0.0.0/16"
}

variable "starterkit_instances_per_availability_zone" {
  default = 1
}

variable "starterkit_instance_type" {
  default = "t2.micro"
}

variable "starterkit_instance_ami" {
  default = {
    # Container Linux (CoreOS) 1520.6.0 Stable Channel (HVM)
    # https://coreos.com/os/docs/latest/booting-on-ec2.html
    us-west-2 = "ami-65269d1d"
  }
}

variable "starterkit_instance_key_name" {
  default = "starterkit-instance"
}

variable "starterkit_bastion_type" {
  default = "t2.micro"
}

variable "starterkit_bastion_ami" {
  default = {
    # Amazon Linux AMI 2017.09.1 (HVM), SSD Volume Type
    us-west-2 = "ami-f2d3638a"
  }
}

variable "starterkit_bastion_key_name" {
  default = "starterkit-bastion"
}
