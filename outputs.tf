# -*- coding: utf-8; mode: terraform; -*-

output "starterkit_bastion_public_ip_address" {
  value = "${aws_instance.starterkit_bastion.public_ip}"
}

output "starterkit_instance_private_ip_addresses" {
  value = "${join(", ", aws_instance.starterkit_instance.*.private_ip)}"
}

output "starterkit_elb_dns_name" {
  value = "${aws_elb.starterkit_elb.dns_name}"
}

output "starterkit_route53_zone_name_servers" {
  value = "${join(", ", aws_route53_zone.starterkit_route53_zone.name_servers)}"
}
