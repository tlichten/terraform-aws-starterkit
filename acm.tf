# -*- coding: utf-8; mode: terraform; -*-

data "aws_acm_certificate" "starterkit_acm_certificate" {
  domain   = "www.${var.starterkit_domain}"
  statuses = ["ISSUED"]
}
