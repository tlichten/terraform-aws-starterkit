# -*- coding: utf-8; mode: make; -*-

SHELL = bash

.PHONY: .sshrc all check-variables-defined create describe-availability-zones destroy format init is-defined-% lint request-certificate show-outputs ssh-% ssh-add-keys upgrade

all: lint create

is-defined-%:
	@$(if $(value $*),,$(error The environment variable $* is undefined))

check-variables-defined: is-defined-AWS_ACCESS_KEY_ID is-defined-AWS_DEFAULT_REGION is-defined-AWS_SECRET_ACCESS_KEY is-defined-STARTERKIT_DOMAIN

init: check-variables-defined
	@terraform init

upgrade: check-variables-defined
	@terraform init -upgrade

format:
	@terraform fmt

lint: init
	@terraform validate							\
	    -var=starterkit_region="$(AWS_DEFAULT_REGION)"			\
	    -var=starterkit_domain="$(STARTERKIT_DOMAIN)"			\
	    .

create: lint
	@terraform plan -out=terraform.plan					\
	    -var=starterkit_region="$(AWS_DEFAULT_REGION)"			\
	    -var=starterkit_domain="$(STARTERKIT_DOMAIN)"			\
	    .
	@bash --init-file .helpers.sh -i -c 'unless_yes "Apply this plan?"'
	@terraform apply terraform.plan

destroy: lint
	@terraform destroy							\
	    -var=starterkit_region="$(AWS_DEFAULT_REGION)"			\
	    -var=starterkit_domain="$(STARTERKIT_DOMAIN)"			\
	    .

show-outputs:
	@terraform output

describe-availability-zones: check-variables-defined
	@aws ec2 describe-availability-zones

request-certificate: check-variables-defined
	@aws acm request-certificate --domain-name www.$(STARTERKIT_DOMAIN) --subject-alternative-names $(STARTERKIT_DOMAIN)

.sshrc: is-defined-STARTERKIT_BASTION_IP_ADDR is-defined-STARTERKIT_INSTANCE_IP_ADDR
	@echo -en							       "\
	  Host *							     \\n\
	    LogLevel quiet						     \\n\
	    StrictHostKeyChecking no					     \\n\
	    UserKnownHostsFile /dev/null				     \\n\
	  Host starterkit-bastion					     \\n\
	    HostName $(STARTERKIT_BASTION_IP_ADDR)			     \\n\
	    User ec2-user						     \\n\
	  Host starterkit-instance					     \\n\
	    HostName $(STARTERKIT_INSTANCE_IP_ADDR)			     \\n\
	    ProxyCommand ssh -F .sshrc starterkit-bastion -W %h:%p	     \\n\
	    User core					      		     \\n\
	" > $@

ssh-add-keys: is-defined-STARTERKIT_BASTION_SSH_PRIVKEY is-defined-STARTERKIT_INSTANCE_SSH_PRIVKEY
	@for SSH_PRIVKEY in "$$STARTERKIT_BASTION_SSH_PRIVKEY" "$$STARTERKIT_INSTANCE_SSH_PRIVKEY";	\
	do									\
	  echo "$$SSH_PRIVKEY" | grep . - | ssh-add - > /dev/null 2>&1;		\
	done

ssh-%: .sshrc ssh-add-keys
	@ssh -F .sshrc starterkit-$*
