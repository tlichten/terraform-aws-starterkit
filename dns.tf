# -*- coding: utf-8; mode: terraform; -*-

resource "aws_route53_zone" "starterkit_route53_zone" {
  name = "${var.starterkit_domain}"

  tags {
    Name = "starterkit-route53-zone"
  }
}

resource "aws_route53_record" "starterkit_route53_record_naked" {
  name    = "${var.starterkit_domain}"
  type    = "A"
  zone_id = "${aws_route53_zone.starterkit_route53_zone.zone_id}"

  alias {
    evaluate_target_health = true
    name                   = "${aws_elb.starterkit_elb.dns_name}"
    zone_id                = "${aws_elb.starterkit_elb.zone_id}"
  }
}

resource "aws_route53_record" "starterkit_route53_record_www" {
  name    = "www.${var.starterkit_domain}"
  records = ["${var.starterkit_domain}"]
  ttl     = "300"
  type    = "CNAME"
  zone_id = "${aws_route53_zone.starterkit_route53_zone.zone_id}"
}
